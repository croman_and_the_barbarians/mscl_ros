cmake_minimum_required(VERSION 2.8.3)
project(mscl_ros)

LINK_DIRECTORIES(/usr/share/c++-mscl)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  sensor_msgs
  mscl_msgs
)


catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES 
#  CATKIN_DEPENDS roscpp std_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########

file(GLOB SRCS
  "include/mscl_ros/*.h"
  "include/mscl_ros/*.cpp"
)

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  "/usr/share/c++-mscl/source"
  "/usr/share/c++-mscl/source/mscl/Communication"
)

add_executable(mscl_ros_node src/mscl_ros_node.cpp ${SRCS})
add_dependencies(mscl_ros_node mscl_msgs_generate_messages_cpp)
target_link_libraries (mscl_ros_node  mscl ${catkin_LIBRARIES} )

