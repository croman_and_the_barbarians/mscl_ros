
#include "../include/mscl_ros/mscl_node.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mscl_ros_node");
    while (ros::ok()) {
        mscl_ros::MSCLNode mscl_node;
        mscl_node.spin();
        ros::Duration(5.0).sleep();
    }
}

/*

#include <iostream>
using namespace std;

#define UNIX_BUILD 1
#include "mscl/MicroStrain/MIP/Commands/GPSTimeUpdate.h"
#include "mscl/mscl.h"
#include "../include/mscl_ros/gps_time_convert.h"

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

//#include "getCurrentConfig.h"
//#include "parseData.h"
#include "../include/mscl_ros/setCurrentConfig.h"
#include "../include/mscl_ros/startSampling.h"
#include "../include/mscl_ros/gps_time_convert.h"
#include "../include/mscl_ros/mscl_node.h"
//#include "setToIdle.h"
#include <chrono>
#include <type_traits>

#include <ros/ros.h>


boost::asio::io_service io;
using namespace std::chrono;



void sendGpsTime(const boost::system::error_code& )
{
  std::cerr << "sending time!" << std::endl;

  boost::asio::deadline_timer t(io, boost::posix_time::seconds(1));
  t.async_wait(&sendGpsTime);
}

std::chrono::system_clock::rep time_since_epoch(){
    static_assert(
        std::is_integral<std::chrono::system_clock::rep>::value,
        "Representation of ticks isn't an integral value."
    );
    auto now = std::chrono::system_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(now).count();
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "mscl_ros_node");
    ros::NodeHandle nh;

    //boost::asio::deadline_timer t(io, boost::posix_time::seconds(1));

    //t.async_wait(&sendGpsTime);

    io.run();
    //TODO: change these constants to match your setup
    const string COM_PORT = "/dev/ttyUSB2";
    std::cerr << "sending time!" << std::endl;
    try
    {
        //create a SerialConnection with the COM port
        mscl::Connection connection = mscl::Connection::Serial(COM_PORT,115200);

        //create an InertialNode with the connection
        mscl::InertialNode node(connection);

        std::cout << "Node Information: " << endl;
        std::cout << "Model Name: " << node.modelName() << endl;
        std::cout << "Model Number: " << node.modelNumber() << endl;
        std::cout << "Serial: " << node.serialNumber() << endl;
        std::cout << "Firmware: " << node.firmwareVersion().str() << endl << endl;

        //TODO: Uncomment the lines below to run the examples

        //Example: Get Configuration
        //getCurrentConfig(node);

        //Example: Set Configuration
        std::cout << "setting config" << endl << endl;
        setCurrentConfig(node);       //Warning: this example changes settings on your Node!

        //Example: Start Sampling
        std::cout << "start sampling" << endl << endl;
        startSampling(node);

        //Example: Set to Idle
        //setToIdle(node);

        //Example: Parse Data
        //parseData(node);

//        mscl::GPSTimeUpdate timeUpdateCmd;
//        timeUpdateCmd.SetWeekValue(10);
//        timeUpdateCmd.SetSecondsValue(10);

//        node.setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_WEEKS,2045);
//        node.setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_SECONDS,426976);
        int last_tow=0;
        GPSTime gps_time = getGpsTime(ros::Time::now().toSec());
        //gps_time.tow+=878244096
        while(true)
        {
            //get all the packets that have been collected, with a timeout of 500 milliseconds
            mscl::MipDataPackets packets = node.getDataPackets(500);

            GPSTime gps_time = getGpsTime(ros::Time::now().toSec()+0.1);
            //std::cout << last_tow << ", " << int(gps_time.tow);
            if(last_tow<int(gps_time.tow)){
                std::cout << std::endl << std::endl << "sending time: " << ros::Time::now() << "," << gps_time.week << "," << gps_time.tow << std::endl;
                //node.setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_WEEKS,gps_time.week);
                //node.setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_SECONDS,gps_time.tow);
                last_tow = int(gps_time.tow);
            }

            for(mscl::MipDataPacket packet : packets)
            {
                unsigned long rx_time = time_since_epoch();
                //std::cout << "---------------------------" << std::endl;
                packet.descriptorSet(); //the descriptor set of the packet

                //on_sec=false;

                mscl::MipTypes::CH_FIELD_SENSOR_GPS_CORRELATION_TIMESTAMP;
                mscl::MipDataPoints points = packet.data();

                for(mscl::MipDataPoint dataPoint : points)
                {
                      //the name of the channel for this point
                    std::cout << dataPoint.channelName() << std::endl;
                    cout << dataPoint.as_string() << std::endl;
//                    if(dataPoint.channelName()=="gpsCorrelTimestampFlags"){
//                        std::cout << dataPoint.channelName() << std::endl;
//                        cout << dataPoint.as_string() << std::endl;
//                    }

                    if(dataPoint.channelName()=="gpsCorrelTimestampTow"){

                        mscl::Timestamp stamp = packet.deviceTimestamp();
                        std::cout << stamp.nanoseconds() << ", "<< time_since_epoch() << std::endl;
                        std::cout << "current time - message time in s: " << (double(rx_time) - double(stamp.nanoseconds()))*1e-9<< std::endl;

                    }
                    //dataPoint.storedAs();     //the ValueType that the data is stored as
                    //dataPoint.as_float();     //get the value as a float
                }
            }
        }
    }
    catch(mscl::Error& e)
    {
        cout << "Error: " << e.what() << endl;
    }


//    system("pause");
    return 0;
}

*/
