#include "gps_time.h"


gpstime::GPSTime gpstime::getGpsTime2(double utc_double){
    GPSTime out;
    double gps_time;
    double tow;
    unsigned long week;
    double current_leap_offset = CURRENT_LEAP_SECCONDS-LEAP_SECCONDS_1980;
    gps_time = utc_double-GPS_TIME_OFFSET+current_leap_offset;
    tow = std::fmod(gps_time,SEC_IN_WEEK);
    week = ulong(gps_time/SEC_IN_WEEK);

    if(utc_double>LEAP_SECCONDS_EXPIRE){
        ROS_WARN_THROTTLE(1.0,"leap second data has expired!  Please update leap second info in gps_time.h");
    }

    out.tow=tow;
    out.week=week;
    return out;
}
