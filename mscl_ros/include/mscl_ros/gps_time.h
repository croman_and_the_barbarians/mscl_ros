#ifndef GPS_TIME_H
#define GPS_TIME_H

#include <cstdio>
#include <unistd.h>
#include <cmath>
#include <chrono>
#include <ctime>
#include <ros/ros.h>


#define LEAP_SECCONDS_1980	19.0
/// update these values after the expiration date
/// @note VALID THROUGH 28 June 2019
#define CURRENT_LEAP_SECCONDS	37.0
#define LEAP_SECCONDS_EXPIRE    1593302400.0  //28 June 2019 in unix time  source: https://www.ietf.org/timezones/data/leap-seconds.list

#define SEC_IN_WEEK             604800.0
#define GPS_TIME_OFFSET         315964800.0
namespace gpstime {


struct GPSTime{
    unsigned long week;
    double long tow;
};

GPSTime getGpsTime2(double utc_double);
}

#endif // GPS_TIME_H
