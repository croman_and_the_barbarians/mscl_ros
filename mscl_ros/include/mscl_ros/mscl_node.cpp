#include "mscl_node.h"

namespace mscl_ros {



MSCLNode::MSCLNode()
{
    ros_node_ptr_.reset(new ros::NodeHandle("~"));
    params_.fromServer(ros_node_ptr_);
    gps_time = gpstime::getGpsTime2(ros::Time::now().toSec());
    last_tow=0;
}

void MSCLNode::connect(){
    mscl_connection_.reset(new mscl::Connection (mscl::Connection::Serial(params_.port,params_.baudrate)));
    mscl_node_.reset(new mscl::InertialNode(*mscl_connection_));
    ROS_INFO("MSCL node '%s' created with device info: \n"
             "        Model Name: %s \n"
             "        Model Number: %s \n"
             "        Serial: %s \n"
             "        Firmware: %s \n",
             ros::this_node::getName().c_str(), mscl_node_->modelName().c_str(),mscl_node_->modelNumber().c_str(),mscl_node_->serialNumber().c_str(),mscl_node_->firmwareVersion().str().c_str());
}

void MSCLNode::setupPublishers(){
    if(params_.imu.imu_topic!="")
        imu_pub_ = ros_node_ptr_->advertise<sensor_msgs::Imu>(params_.imu.imu_topic,100);
    if(params_.imu.mag_topic!="")
        mag_pub_ = ros_node_ptr_->advertise<sensor_msgs::MagneticField>(params_.imu.mag_topic,100);
    if(params_.imu.gps_cor_topic!="")
        gps_cor_pub_ = ros_node_ptr_->advertise<mscl_msgs::GpsCorrelationTimestampStamped>(params_.imu.gps_cor_topic,100);
    if(params_.imu.gps_cor_topic!="")
        delta_imu_pub_ = ros_node_ptr_->advertise<sensor_msgs::Imu>(params_.imu.delta_imu_topic,100);
}

void MSCLNode::setCurrentConfig(){
    ROS_INFO("Setting current config");
    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_AHRS_IMU))
    {
        mscl::MipChannels ahrsImuChs;
        // get timestamps
        if(params_.imu.imu_topic!=""||params_.imu.mag_topic!=""||params_.imu.delta_imu_topic!=""||params_.imu.gps_cor_topic!="")
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_GPS_CORRELATION_TIMESTAMP, mscl::SampleRate::Hertz(params_.imu.rate)));
        // raw types
        if(params_.imu.imu_topic!=""){
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_SCALED_ACCEL_VEC, mscl::SampleRate::Hertz(params_.imu.rate)));
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_SCALED_GYRO_VEC, mscl::SampleRate::Hertz(params_.imu.rate)));
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_ORIENTATION_QUATERNION, mscl::SampleRate::Hertz(params_.imu.rate)));
        }
        if(params_.imu.mag_topic!=""){
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_SCALED_MAG_VEC, mscl::SampleRate::Hertz(params_.imu.rate)));
        }
        // deltas
        if(params_.imu.delta_imu_topic!=""){
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_DELTA_THETA_VEC, mscl::SampleRate::Hertz(params_.imu.rate)));
            ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_DELTA_VELOCITY_VEC, mscl::SampleRate::Hertz(params_.imu.rate)));
        }
        //apply to the node
        mscl_node_->setActiveChannelFields(mscl::MipTypes::CLASS_AHRS_IMU, ahrsImuChs);
    }

    //if the node supports Estimation Filter
    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_ESTFILTER))
    {
        mscl::MipChannels estFilterChs;
        //estFilterChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_ESTFILTER_ESTIMATED_GYRO_BIAS, mscl::SampleRate::Hertz(100)));

        //apply to the node
        mscl_node_->setActiveChannelFields(mscl::MipTypes::CLASS_ESTFILTER, estFilterChs);
    }

    //if the node supports GNSS
    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_GNSS))
    {
        mscl::MipChannels gnssChs;
        //gnssChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_GNSS_LLH_POSITION, mscl::SampleRate::Hertz(1)));

        //apply to the node
        mscl_node_->setActiveChannelFields(mscl::MipTypes::CLASS_GNSS, gnssChs);
        mscl_node_->setAltitudeAid(false);
        mscl::PositionOffset offset(0.0f, 0.0f, 0.0f);
        mscl_node_->setAntennaOffset(offset);
    }

    mscl_node_->setPitchRollAid(true);

}

void MSCLNode::startSampling(){
    ROS_INFO("Starting sampling");
    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_AHRS_IMU))
    {
        mscl_node_->enableDataStream(mscl::MipTypes::CLASS_AHRS_IMU);
    }

    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_ESTFILTER))
    {
        mscl_node_->enableDataStream(mscl::MipTypes::CLASS_ESTFILTER);
    }

    if(mscl_node_->features().supportsCategory(mscl::MipTypes::CLASS_GNSS))
    {
        mscl_node_->enableDataStream(mscl::MipTypes::CLASS_GNSS);
    }
}

void MSCLNode::sendTime(){
    if(params_.use_external_pps){
        gpstime::GPSTime gps_time = gpstime::getGpsTime2(ros::Time::now().toSec()-.1);
        if(last_tow<int(gps_time.tow)){
            std::cout << std::endl << std::endl << "sending time: " << ros::Time::now() << "," << gps_time.week << "," << gps_time.tow << std::endl;
            mscl_node_->setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_WEEKS,gps_time.week);
            mscl_node_->setGPSTimeUpdate(mscl::MipTypes::TimeFrame::TIME_FRAME_SECONDS,gps_time.tow);
            last_tow = int(gps_time.tow);
        }
    }
}


void MSCLNode::spin_once(){


    sendTime();

    mscl::MipDataPackets packets = mscl_node_->getDataPackets(500);
    for(mscl::MipDataPacket packet : packets)
    {
        double dt=ros::Time::now().toSec() - double(packet.deviceTimestamp().nanoseconds())*1e-9;
        if(dt<0-params_.warn_future){
            ROS_WARN_THROTTLE(1.0,"%s received a message from the microstrain that was %f sec in the future",ros::this_node::getName().c_str(),-dt);
        }
        if(dt>params_.warn_old){
            ROS_WARN_THROTTLE(1.0,"%s received a message from the microstimu_msg.linear_acceleration.xrain that was %f sec in the past",ros::this_node::getName().c_str(),dt);
        }

        if(packet.descriptorSet()==mscl::MipTypes::CLASS_AHRS_IMU){
            imuMsgCallback(packet);
        }


    }
}


void MSCLNode::spin(){

    while(ros::ok()){
        try{
            connect();
            setupPublishers();
            setCurrentConfig();
            while(ros::ok()){
                spin_once();
            }
        }
        catch(mscl::Error& e)
        {
            ROS_ERROR("MSCL Error: %s -- restarting...", e.what());
            ros::Duration(5.0).sleep();
        }
    }

}


void MSCLNode::imuMsgCallback(mscl::MipDataPacket &packet){
    mscl::MipDataPoints points = packet.data();

    // setup headder shared by all message generated from MIP imu data
    std_msgs::Header shared_hdr;
    shared_hdr.stamp.fromNSec(packet.deviceTimestamp().nanoseconds());
    shared_hdr.frame_id = params_.frame_id;


    // define ros_messages and set header
    sensor_msgs::Imu imu_msg,delta_imu_msg;
    imu_msg.header = shared_hdr;
    delta_imu_msg.header = shared_hdr;
    sensor_msgs::MagneticField mag_msg;
    mag_msg.header = shared_hdr;
    mscl_msgs::GpsCorrelationTimestampStamped gps_cor_msg;
    gps_cor_msg.header = shared_hdr;


    // itterate through the data packet and pack the ros messages
    for(size_t i = 0; i< points.size() ;i++)
    {
        mscl::MipDataPoint dataPoint = points[i];
        switch(dataPoint.field()){

        case mscl::MipTypes::CH_FIELD_SENSOR_SCALED_ACCEL_VEC:{
            mipXYZ2ros(dataPoint,imu_msg.linear_acceleration,USTRAIN_G);
        }break;

        case mscl::MipTypes::CH_FIELD_SENSOR_SCALED_GYRO_VEC:{
            mipXYZ2ros(dataPoint,imu_msg.angular_velocity);
        }break;

        case mscl::MipTypes::CH_FIELD_SENSOR_ORIENTATION_QUATERNION:{
            // put into ENU - swap X/Y, invert Z
            imu_msg.orientation.w =  double(dataPoint.as_Vector().as_floatAt(0));
            imu_msg.orientation.x =  double(dataPoint.as_Vector().as_floatAt(2));
            imu_msg.orientation.y =  double(dataPoint.as_Vector().as_floatAt(1));
            imu_msg.orientation.z =  double(-dataPoint.as_Vector().as_floatAt(3));
        }break;

        case mscl::MipTypes::CH_FIELD_SENSOR_SCALED_MAG_VEC:{
            mipXYZ2ros(dataPoint,mag_msg.magnetic_field);
        }break;

        case mscl::MipTypes::CH_FIELD_SENSOR_GPS_CORRELATION_TIMESTAMP:{
            gps_cor_msg.gps_cor.gps_tow = points[i].as_double();
            gps_cor_msg.gps_cor.gps_week_number = points[i+1].as_uint16();
            gps_cor_msg.gps_cor.timestamp_flags = points[i+2].as_uint16();
            i=i+2;
            //todo: add status flag stuff here
        }break;


        case mscl::MipTypes::CH_FIELD_SENSOR_DELTA_THETA_VEC:{
            mipXYZ2ros(dataPoint,delta_imu_msg.angular_velocity,params_.imu.rate);
        }break;

        case mscl::MipTypes::CH_FIELD_SENSOR_DELTA_VELOCITY_VEC:{
            mipXYZ2ros(dataPoint,delta_imu_msg.linear_acceleration,USTRAIN_G*params_.imu.rate);
        }break;



        default:
            ROS_WARN("Unsupported IMU MIP type encountered:  %s with value: %s", dataPoint.channelName().c_str(),dataPoint.as_string().c_str());

        }

    }

    // pack covariances
    imu_msg.linear_acceleration_covariance[0]=pow(params_.imu.acc_deviation,2);
    imu_msg.linear_acceleration_covariance[4]=pow(params_.imu.acc_deviation,2);
    imu_msg.linear_acceleration_covariance[8]=pow(params_.imu.acc_deviation,2);

    imu_msg.angular_velocity_covariance[0]=pow(params_.imu.gyro_deviation,2);
    imu_msg.angular_velocity_covariance[4]=pow(params_.imu.gyro_deviation,2);
    imu_msg.angular_velocity_covariance[8]=pow(params_.imu.gyro_deviation,2);

    imu_msg.orientation_covariance[0]=pow(params_.imu.rp_deviation,2);
    imu_msg.orientation_covariance[4]=pow(params_.imu.rp_deviation,2);
    imu_msg.orientation_covariance[8]=pow(params_.imu.yaw_deviation,2);

    mag_msg.magnetic_field_covariance[0]=pow(params_.imu.mag_deviation,2);
    mag_msg.magnetic_field_covariance[4]=pow(params_.imu.mag_deviation,2);
    mag_msg.magnetic_field_covariance[8]=pow(params_.imu.mag_deviation,2);

    delta_imu_msg.linear_acceleration_covariance = imu_msg.linear_acceleration_covariance;
    delta_imu_msg.angular_velocity_covariance = imu_msg.angular_velocity_covariance;


    // send messages
    if(params_.imu.imu_topic!="")
        imu_pub_.publish(imu_msg);
    if(params_.imu.mag_topic!="")
        mag_pub_.publish(mag_msg);
    if(params_.imu.gps_cor_topic!="")
        gps_cor_pub_.publish(gps_cor_msg);
    if(params_.imu.delta_imu_topic!=""){
        double diff = delta_imu_msg.header.stamp.toSec() - last_imu_time_.toSec();
        if(abs(diff-1.0/params_.imu.rate) > params_.imu.max_rate_error){  // check if rate is bad
            if(last_imu_time_.toSec() > 0.1)  // check if this is the first message
                ROS_WARN_THROTTLE(1.0,"measured imu period: %fs nominal period: %fs (%fhz)  not sending delta_imu_msg", diff, 1.0/params_.imu.rate,params_.imu.rate);
        }else {
            delta_imu_pub_.publish(delta_imu_msg);
        }
    }

    // update last time
    last_imu_time_ = shared_hdr.stamp;




}

//
// helper functions
//

void mipXYZ2ros(const mscl::MipDataPoint & dataPoint, geometry_msgs::Vector3 & ros_vect, double scale_factor){
    // put into ENU - swap X/Y, invert Z
    if(dataPoint.qualifier()==mscl::MipTypes::ChannelQualifier::CH_X)
        ros_vect.y =  double(dataPoint.as_float())*scale_factor;
    if(dataPoint.qualifier()==mscl::MipTypes::ChannelQualifier::CH_Y)
        ros_vect.x =  double(dataPoint.as_float())*scale_factor;
    if(dataPoint.qualifier()==mscl::MipTypes::ChannelQualifier::CH_Z)
        ros_vect.z = -double(dataPoint.as_float())*scale_factor;
}



}
