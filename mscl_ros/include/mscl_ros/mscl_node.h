#ifndef MSCL_NODE_H
#define MSCL_NODE_H

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <mscl_msgs/GpsCorrelationTimestampStamped.h>

#include <vector>

#include "mscl/MicroStrain/MIP/Commands/GPSTimeUpdate.h"
#include "mscl/mscl.h"
#include "gps_time.h"

#define USTRAIN_G 9.80665

namespace mscl_ros {
struct MSCLParams{
    void fromServer(ros::NodeHandlePtr node_ptr){
        node_ptr->param<std::string>("port",port,"/dev/ttyUSB2");
        node_ptr->param("baudrate",baudrate,115200);
        node_ptr->param<std::string>("frame_id",frame_id,"ustrain_imu");
        node_ptr->param("warn_future",warn_future,0.0);
        node_ptr->param("warn_old",warn_old,0.2);
        node_ptr->param("use_external_pps",use_external_pps,false);

        node_ptr->param<std::string>("imu/imu_topic",imu.imu_topic,"imu/raw");
        node_ptr->param<std::string>("imu/mag_topic",imu.mag_topic,"mag/raw");
        node_ptr->param<std::string>("imu/delta_imu_topic",imu.delta_imu_topic,"imu/delta");
        node_ptr->param<std::string>("imu/gps_cor_topic",imu.gps_cor_topic,"imu/gps_cor");

        node_ptr->param("imu/rate",imu.rate,100.0);
        //the following default params were determined empiraclly from the 3DM-GX5-45 datastream
        node_ptr->param("imu/acc_deviation" ,imu.acc_deviation ,0.0002);
        node_ptr->param("imu/gyro_deviation",imu.gyro_deviation,0.001);
        //the following default params were determined empiraclly from the 3DM-GX5-45 datasheet(https://www.microstrain.com/sites/default/files/applications/files/3dm-gx5-45_datasheet_8400-0091_rev_l.pdf)
        node_ptr->param("imu/mag_deviation" ,imu.mag_deviation ,0.048);
        node_ptr->param("imu/rp_deviation" ,imu.rp_deviation   ,0.035);
        node_ptr->param("imu/yaw_deviation" ,imu.yaw_deviation ,0.035);
        node_ptr->param("imu/max_rate_error",imu.max_rate_error,imu.rate/10.0);

        node_ptr->param<std::string>("est_filter/topic",est_filter.topic,"filter/odom");
        node_ptr->param("est_filter/rate",est_filter.rate,50.0);

        node_ptr->param<std::string>("gnss/topic",gnss.topic,"gnss/fix");
        node_ptr->param("gnss/rate",gnss.rate,50.0);

    }
    std::string port;
    int baudrate;
    std::string frame_id;
    double warn_future;
    double warn_old;
    bool use_external_pps;

    //double imu_rate;
    //double est_filter_rate;
    //double gnss_rate;
    struct{
        std::string imu_topic;
        std::string mag_topic;
        std::string gps_cor_topic;
        std::string delta_imu_topic;
        double rate;
        double acc_deviation;   //.0002 m/s^2
        double gyro_deviation;  //.001 rad/s
        double mag_deviation;   //.048 gauss
        double rp_deviation;
        double yaw_deviation;
        double max_rate_error;
    }imu;
    struct{
        std::string topic;
        double rate;
    }est_filter;
    struct{
        std::string topic;
        double rate;
    }gnss;

};

class MSCLNode{
public:
    MSCLNode();
    void connect();
    void setupPublishers();
    void setCurrentConfig();
    void startSampling();
    void sendTime();
    void spin();
    void spin_once();

protected:
    void imuMsgCallback(mscl::MipDataPacket & packet);
private:
    ros::NodeHandlePtr ros_node_ptr_;
    ros::Publisher imu_pub_;
    ros::Publisher mag_pub_;
    ros::Publisher gps_cor_pub_;
    ros::Publisher delta_imu_pub_;
    ros::Time last_imu_time_;
    std::shared_ptr<mscl::Connection> mscl_connection_;
    std::shared_ptr<mscl::InertialNode> mscl_node_;
    MSCLParams params_;
    gpstime::GPSTime gps_time;
    int last_tow;
};

/*!
 * \brief Converts an XYZ MIP data type to ros XYZ.  In other words it changes the cordinate froma from NED to ENU
 * \param dataPoint[input]  the mscl::MipDataPoint you want to get ros xyz for
 * \param ros_vect[output]
 * \param scale_factor[input]  Optional.  simple multiplier useful for changing units
 */
void mipXYZ2ros(const mscl::MipDataPoint & dataPoint, geometry_msgs::Vector3 &ros_vect, double scale_factor = 1);

}


#endif // MSCL_NODE_H
